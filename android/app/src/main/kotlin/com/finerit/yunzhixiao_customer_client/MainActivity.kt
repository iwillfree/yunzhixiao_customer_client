package com.finerit.yunzhixiao_customer_client

import android.os.Bundle
import androidx.annotation.NonNull;
import com.tencent.bugly.Bugly
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Bugly.init(applicationContext, "38026141d7", false)
    }
}
